<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CiO&0hi QNB<sjvRB2?GT@z^OM]*,&|-EeG:c*z*R9>LZNf5{M=eKvG{vXJ!U4~h');
define('SECURE_AUTH_KEY',  'C9cmvjXi1/!.#9Pp-usS5+F]4*BSlZ2J{@ymOYjPE][:w4rL6:`z9n7(H))%$ld)');
define('LOGGED_IN_KEY',    'WrohtSEoYMK68D[3 r?ov(NH;61JfoamNRPjT6eAA?1*g6xSUyUt,[{}+]h.Ot[v');
define('NONCE_KEY',        'G-Qr@w_  PK OEL3[Lvbho1!e@Ux(.7-$D.H0mHegzV+SNV1_G<q)V2}8X32|t_*');
define('AUTH_SALT',        ')1>FC }Vc-bI{xt;&mbE+.1BDN1q{^^eE#y%7)=`,h^/8CTGEru9.}cjq} led< ');
define('SECURE_AUTH_SALT', 'w6W}WS&Cgb4Y390}Xit:K-:N0rw]ei#j>&3^j$I$?^5;VF[RRbP}(gJFXWc&tDqh');
define('LOGGED_IN_SALT',   'NVc3^&v@z5Y=?gjr=jx:R*]m;c!Q=w!eO&q$Ts!h7hW+qTM c.!x(aZPgW~q<#-%');
define('NONCE_SALT',       'QT_@i%Ify/i<<D1w`Bs%_Gn``,D0;o#{!Z;Fx]hN4H~EsJzMl!T?N8wQF;<?uf[h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
